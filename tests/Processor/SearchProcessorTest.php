<?php
namespace App\Tests\Processor;

use App\Entity\FlyCarrierEntity;
use App\Entity\FlyRouteEntity;
use App\Processor\SearchProcessor;
use App\Repository\FlyRouteRepository;
use PHPUnit\Framework\TestCase;
use App\Entity\Request\Search\SearchPostEntity as SearchRequest;
use App\Entity\Response\Search\SearchPostEntity as SearchResponse;
use Doctrine\Common\Persistence\ManagerRegistry;

class SearchProcessorTest extends TestCase
{
    /**
     * @param SearchRequest $searchRequest
     * @param FlyRouteEntity[] $dbResponse
     * @param SearchResponse $searchResponse
     *
     * @covers \App\Processor\SearchProcessor::searchBy2AirportsAndDate()
     * @dataProvider providerForSearchBy2AirportsAndDate
     */
    public function testSearchBy2AirportsAndDate(
        SearchRequest $searchRequest,
        array $dbResponse,
        SearchResponse $searchResponse
    ) {
        $mockFlyRouteRepository = $this->createMock(FlyRouteRepository::class);
        $mockFlyRouteRepository->method('getRoutesBy2AirportsAndDate')->with(
            $searchRequest->getSearchQuery()['departureAirport'],
            $searchRequest->getSearchQuery()['arrivalAirport'],
            new \DateTime($searchRequest->getSearchQuery()['departureDate'])
        )
            ->willReturn($dbResponse);

        $mockDoctrine = $this->createMock(ManagerRegistry::class);
        $mockDoctrine->method('getRepository')
            ->with(FlyRouteEntity::class)
            ->willReturn($mockFlyRouteRepository);

        $processor = new SearchProcessor($mockDoctrine);
        $result = $processor->searchBy2AirportsAndDate($searchRequest);

        $this->assertEquals($result, $searchResponse);
    }

    /**
     * @case 0 Normal work with one result
     * @case 1 Empty response
     */
    public function providerForSearchBy2AirportsAndDate()
    {
        /* === case 0 === */

        $searchRequest = new SearchRequest();
        $searchRequest->setSearchQuery([
            'departureAirport' => 'KBP',
            'arrivalAirport' => 'BUD',
            'departureDate' => '2019-09-09',
        ]);

        $searchResponse = new SearchResponse();
        $searchResponse->setSearchQuery([
            'departureAirport' => 'KBP',
            'arrivalAirport' => 'BUD',
            'departureDate' => '2019-09-09',
        ]);
        $searchResponse->setSearchResults([
            [
                'transporter' => [
                    'code' => 'TGZ',
                    'name' => 'Georgian Airways',
                ],
                'flightNumber' => 'TGZ01',
                'departureAirport' => 'KBP',
                'arrivalAirport' => 'BUD',
                'departureDateTime' => '2019-09-09 09:00',
                'arrivalDateTime' => '2019-09-09 11:00',
                'duration' => 120
            ],
        ]);

        $carrier = new FlyCarrierEntity();
        $carrier->setCode('TGZ');
        $carrier->setName('Georgian Airways');

        $flyRouteEntity = new FlyRouteEntity();
        $flyRouteEntity->setDepartAirport('KBP');
        $flyRouteEntity->setArrAirport('BUD');
        $flyRouteEntity->setDepartTime(new \DateTime('2019-09-09 09:00'));
        $flyRouteEntity->setArrTime(new \DateTime('2019-09-09 11:00'));
        $flyRouteEntity->setFlightNumber('TGZ01');
        $flyRouteEntity->setCarrier($carrier);

        $result[0] = [
            'searchRequest' => $searchRequest,
            'dbResponse' => [
                $flyRouteEntity
            ],
            'searchResponse' => $searchResponse,
        ];

        /* === case 1 === */

        $searchRequest = new SearchRequest();
        $searchRequest->setSearchQuery([
            'departureAirport' => 'KBP',
            'arrivalAirport' => 'BUD',
            'departureDate' => '2019-09-10',
        ]);

        $searchResponse = new SearchResponse();
        $searchResponse->setSearchQuery([
            'departureAirport' => 'KBP',
            'arrivalAirport' => 'BUD',
            'departureDate' => '2019-09-10',
        ]);
        $searchResponse->setSearchResults([]);

        $result[1] = [
            'searchRequest' => $searchRequest,
            'dbResponse' => [],
            'searchResponse' => $searchResponse,
        ];

        return $result;
    }
}