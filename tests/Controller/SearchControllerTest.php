<?php
namespace App\Tests\Controller;

use App\Controller\SearchController;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;

class SearchControllerTest extends WebTestCase
{
    /**
     * @param string $body
     * @param array $headers
     * @param string $expected
     *
     * @covers SearchController::searchAction
     * @dataProvider providerSearchAction
     */
    public function testSearchAction(string $body, array $headers, string $expected)
    {
        $client = static::createClient([], $headers);

        $client->request(
            Request::METHOD_POST,
            '/api/search',
            [],
            [],
            [],
            $body
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals($expected, $client->getResponse()->getContent());
    }

    /**
     * @case 0 Normal mode with one result
     */
    public function providerSearchAction()
    {
        return [
            [
                'body' => '{
       "searchQuery": {
           "departureAirport": "KBP",
           "arrivalAirport": "BUD",
           "departureDate": "2019-09-09"
       }
    }',
                'headers' => [
                    'CONTENT-TYPE' => 'application/json',
                    'HTTP_AUTHORIZATION' => 'Basic Zmlyc3RVc2VyOk15U2VjcmV0UGFzc3dvcmQ='
                ],
                'expected' => '{"searchQuery":{"departureAirport":"KBP","arrivalAirport":"BUD","departureDate":"2019-09-09"},"searchResults":[{"transporter":{"code":"TGZ","name":"Georgian Airways"},"flightNumber":"TGZ01","departureAirport":"KBP","arrivalAirport":"BUD","departureDateTime":"2019-09-09 09:00","arrivalDateTime":"2019-09-09 11:00","duration":120}]}',
            ],
        ];
    }
}