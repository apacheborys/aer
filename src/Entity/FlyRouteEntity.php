<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="fly_routes", indexes={
 *     @ORM\Index(name="depart_airport_idx", columns={"depart_airport"}),
 *     @ORM\Index(name="depart_time_idx", columns={"depart_time"}),
 *     @ORM\Index(name="arr_airport_idx", columns={"arr_airport"}),
 *     @ORM\Index(name="arr_time_idx", columns={"arr_time"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\FlyRouteRepository")
 */
class FlyRouteEntity
{
    /**
     * @ORM\Column(type="string", name="flight_number", length=8)
     * @ORM\Id
     * @Assert\NotBlank()
     * @var string $flightNumber
     */
    private $flightNumber;

    /**
     * @ORM\OneToOne(targetEntity="FlyCarrierEntity")
     * @ORM\JoinColumn(name="carrier_id", referencedColumnName="code")
     * @var FlyCarrierEntity $carrier
     */
    private $carrier;

    /**
     * @ORM\Column(type="string", name="depart_airport", length=4)
     * @Assert\NotBlank()
     * @var string $departAirport
     */
    private $departAirport;

    /**
     * @ORM\Column(type="datetimetz", name="depart_time")
     * @Assert\NotBlank()
     * @var \DateTime $departTime
     */
    private $departTime;

    /**
     * @ORM\Column(type="string", name="arr_airport", length=4)
     * @Assert\NotBlank()
     * @var string $arrAirport
     */
    private $arrAirport;

    /**
     * @ORM\Column(type="datetimetz", name="arr_time")
     * @var \DateTime $arrTime
     */
    private $arrTime;

    /**
     * @return string
     */
    public function getFlightNumber(): string
    {
        return $this->flightNumber;
    }

    /**
     * @param string $flightNumber
     */
    public function setFlightNumber(string $flightNumber): void
    {
        $this->flightNumber = $flightNumber;
    }

    /**
     * @return string
     */
    public function getDepartAirport(): string
    {
        return $this->departAirport;
    }

    /**
     * @param string $departAirport
     */
    public function setDepartAirport(string $departAirport): void
    {
        $this->departAirport = $departAirport;
    }

    /**
     * @return \DateTime
     */
    public function getDepartTime(): \DateTime
    {
        return $this->departTime;
    }

    /**
     * @param \DateTime $departTime
     */
    public function setDepartTime(\DateTime $departTime): void
    {
        $this->departTime = $departTime;
    }

    /**
     * @return string
     */
    public function getArrAirport(): string
    {
        return $this->arrAirport;
    }

    /**
     * @param string $arrAirport
     */
    public function setArrAirport(string $arrAirport): void
    {
        $this->arrAirport = $arrAirport;
    }

    /**
     * @return \DateTime
     */
    public function getArrTime(): \DateTime
    {
        return $this->arrTime;
    }

    /**
     * @param \DateTime $arrTime
     */
    public function setArrTime(\DateTime $arrTime): void
    {
        $this->arrTime = $arrTime;
    }

    /**
     * @return FlyCarrierEntity
     */
    public function getCarrier(): FlyCarrierEntity
    {
        return $this->carrier;
    }

    /**
     * @param FlyCarrierEntity $carrier
     */
    public function setCarrier(FlyCarrierEntity $carrier): void
    {
        $this->carrier = $carrier;
    }
}
