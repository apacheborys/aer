<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="fly_carrier")
 */
class FlyCarrierEntity
{
    /**
     * @ORM\Column(type="string", name="code", length=8)
     * @ORM\Id
     * @Assert\NotBlank()
     * @var string $code
     */
    private $code;

    /**
     * @ORM\Column(type="string", name="name", length=126)
     * @Assert\NotBlank()
     * @var string $name
     */
    private $name;

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }
}