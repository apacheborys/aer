<?php
namespace App\Entity\Response\Search;

use App\Entity\FlyRouteEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class SearchPostEntity
{
    /**
     * @var array $searchQuery
     */
    private $searchQuery;

    /**
     * @var array $searchResults
     */
    private $searchResults;

    /**
     * @return array
     */
    public function getSearchQuery(): array
    {
        return $this->searchQuery;
    }

    /**
     * @param array $searchQuery
     */
    public function setSearchQuery(array $searchQuery): void
    {
        $this->searchQuery = $searchQuery;
    }

    /**
     * @return array
     */
    public function getSearchResults(): array
    {
        return $this->searchResults;
    }

    /**
     * @param array $searchResults
     */
    public function setSearchResults(array $searchResults): void
    {
        $this->searchResults = $searchResults;
    }

    /**
     * Filling data in searchResults property from array which contain FlyRouteEntity
     *
     * @var FlyRouteEntity[] $flyRouteEntities
     */
    public function setSearchResultsFromFlyRoute(array $flyRouteEntities)
    {
        foreach ($flyRouteEntities as $flyRouteEntity) {
            $this->searchResults[] = [
                'transporter' => [
                    'code' => $flyRouteEntity->getCarrier()->getCode(),
                    'name' => $flyRouteEntity->getCarrier()->getName(),
                ],
                'flightNumber' => $flyRouteEntity->getFlightNumber(),
                'departureAirport' => $flyRouteEntity->getDepartAirport(),
                'arrivalAirport' => $flyRouteEntity->getArrAirport(),
                'departureDateTime' => $flyRouteEntity->getDepartTime()->format('Y-m-d H:i'),
                'arrivalDateTime' => $flyRouteEntity->getArrTime()->format('Y-m-d H:i'),
                'duration' =>
                    (int)round(($flyRouteEntity->getArrTime()->getTimestamp() -
                        $flyRouteEntity->getDepartTime()->getTimestamp()) / 60)
            ];
        }
    }

    /**
     * Return final result as array
     *
     * @return array
     */
    public function getResult()
    {
        return [
            'searchQuery' => $this->searchQuery,
            'searchResults' => $this->searchResults,
        ];
    }
}