<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class UserAPIEntity
 * @package App\Entity
 * @ORM\Entity
 * @ORM\Table(name="users", uniqueConstraints={@ORM\UniqueConstraint(name="main_idx", columns={"login", "password"})})
 */
class UserAPIEntity
{
    /**
     * @ORM\Column(type="string", name="login", length=32)
     * @ORM\Id
     * @Assert\NotBlank
     * @var string $login
     */
    private $login;

    /**
     * @ORM\Column(type="string", name="password", length=126)
     * @ORM\Id
     * @Assert\NotBlank
     * @var string $password
     */
    private $password;

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }
}