<?php
namespace App\Entity\Request\Search;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class SearchPostEntity
{
    /**
     * @var array $searchQuery
     */
    private $searchQuery;

    /**
     * @return array
     */
    public function getSearchQuery(): array
    {
        return $this->searchQuery;
    }

    /**
     * @param array $searchQuery
     */
    public function setSearchQuery(array $searchQuery): void
    {
        $this->searchQuery = $searchQuery;
    }

    /**
     * @Assert\Callback
     * @param ExecutionContextInterface $context
     * @param $payload
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        /**
         * Check for isset all elements
         */
        foreach (['departureAirport', 'arrivalAirport', 'departureDate'] as $field) {
            if (!isset($this->searchQuery[$field])) {
                $context->buildViolation('Field ' . $field . ' is absent in searchQuery element')
                    ->atPath('searchQuery')
                    ->addViolation();
            }
        }

        /**
         * Check for elements quantity
         */
        if (count($this->searchQuery) != 3) {
            $context->buildViolation('Unexpected quantity of fields')
                ->atPath('searchQuery')
                ->addViolation();
        }

        /**
         * Check for date pattern. For example 2018-07-01
         */
        $pattern = '/^[0-9]{4}-(((0[13578]|(10|12))-'
            .'(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)'
            .'-(0[1-9]|[1-2][0-9]|30)))$/';

        if (isset($this->searchQuery['departureDate']) && !preg_match($pattern, $this->searchQuery['departureDate'])) {
            $context->buildViolation('Field departureDate have incorrect format')
                ->atPath('searchQuery')
                ->addViolation();
        }
    }
}