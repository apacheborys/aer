<?php
namespace App\EventSubscriber;

use App\Entity\UserAPIEntity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class RequestSubscriber
 * @package App\EventSubscriber
 *
 * Authorization layer. Each user must call to this API interface with 'Authorization' header. In this header must be
 * next pattern - 'Basic YOUR_ENCODED_TOKEN'. YOUR_ENCODED_TOKEN must be computed in next steps:
 * 1. Concat user login, semicolon (:) and password
 * 2. Encode result string in base64 string
 * 3. Result string please send above YOUR_ENCODED_TOKEN
 */
class RequestSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    )
    {
        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [
                ['processRequest'],
            ],
        ];
    }

    /**
     * @param RequestEvent $event
     */
    public function processRequest(RequestEvent $event):void
    {
        $dispatch = explode('/', $event->getRequest()->getRequestUri());

        if (isset($dispatch[1]) && $dispatch[1] != 'api') {
            return;
        }

        $encodedData = $event->getRequest()->headers->get('Authorization');

        if (!$encodedData) {
            $event->setResponse(
                $this->makeResponse(
                    json_encode(['Can\'t find Authorization header']),
                    Response::HTTP_UNAUTHORIZED
                )
            );
            return;
        }

        $basicMarker = strpos($encodedData, 'Basic');

        if ($basicMarker === false) {
            $event->setResponse(
                $this->makeResponse(
                    json_encode(['Can\'t find Basic prefix in content of Authorization header']),
                    Response::HTTP_UNAUTHORIZED
                )
            );
            return;
        }

        $decodedData = base64_decode(substr($encodedData, 6));

        if (!$decodedData) {
            $event->setResponse(
                $this->makeResponse(
                    json_encode(['Can\'t decode content of Authorization header']),
                    Response::HTTP_UNAUTHORIZED
                )
            );
            return;
        }

        $semicolon = strpos($decodedData, ':');

        if ($semicolon === false) {
            $event->setResponse(
                $this->makeResponse(
                    json_encode(['Can\'t find semicolon in content of Authorization header']),
                    Response::HTTP_UNAUTHORIZED
                )
            );
            return;
        }

        $userRepository = $this->entityManager->getRepository(UserAPIEntity::class);
        $userInDb = $userRepository->findOneBy([
            'login' => substr($decodedData, 0, $semicolon),
            'password' => md5(substr($decodedData, $semicolon + 1)),
        ]);

        if (!$userInDb) {
            $event->setResponse(
                $this->makeResponse(json_encode(['Can\'t find user in db']), Response::HTTP_UNAUTHORIZED)
            );
            return;
        }

        putenv('USER=' . $userInDb->getLogin());

        return;
    }

    /**
     * @param string $content
     * @param int $statusCode
     * @return Response
     */
    public function makeResponse(string $content, int $statusCode):Response
    {
        $response = new Response();
        $response->setContent($content);
        $response->setStatusCode($statusCode);

        return $response;
    }
}