<?php
namespace App\Controller;

use App\Entity\FlyRouteEntity;
use App\Entity\Request\Search\SearchPostEntity as SearchRequest;
use App\Entity\Response\Search\SearchPostEntity as SearchResponse;
use App\Processor\SearchProcessor;
use App\Repository\FlyRouteRepository;
use Doctrine\Common\Persistence\ObjectRepository;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Bridge\Doctrine\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Search controller.
 * @Route("/api", name="api_")
 */
class SearchController extends AbstractFOSRestController
{
    /**
     * @var ManagerRegistry $doctrine
     */
    public $doctrine;

    /**
     * @param ManagerRegistry $doctrine
     */
    public function setDoctrine(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * Lists all Routes.
     * @SWG\Post(
     *     path="/api/search",
     *     summary="Make searching",
     *     description="Make searching through departure airport, arrival airport and departure date",
     *     operationId="api_search",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *          name="Authorization",
     *          in="header",
     *          required=true,
     *          type="string",
     *          default="Basic TOKEN",
     *          description="Please make properly basic authorization according to standard",
     *     ),
     *     @SWG\Parameter(
     *          name="Request json",
     *          in="body",
     *          required=true,
     *          type="application/json",
     *          description="Request data model",
     *          @SWG\Schema(type="object",
     *              @SWG\Property(property="searchQuery", type="object",
     *                  @SWG\Property(property="departureAirport", type="string"),
     *                  @SWG\Property(property="arrivalAirport", type="string"),
     *                  @SWG\Property(property="departureDate", type="string"),
     *              ),
     *              example={
     *                  "application/json":{
     *                      {
     *                          "searchQuery": {
     *                              "departureAirport": "KBP",
     *                              "arrivalAirport": "BUD",
     *                              "departureDate": "2018-07-01"
     *                          }
     *                      }
     *                  },
     *              },
     *          ),
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Success",
     *         @SWG\Schema(type="object",
     *              @SWG\Property(property="searchQuery", type="object",
     *                  @SWG\Property(property="departureAirport", type="string"),
     *                  @SWG\Property(property="arrivalAirport", type="string"),
     *                  @SWG\Property(property="departureDate", type="string"),
     *              ),
     *              @SWG\Property(property="searchResults", type="array",
     *                  @SWG\Items(type="object",
     *                      @SWG\Property(property="transporter", type="object",
     *                          @SWG\Property(property="code", type="string"),
     *                          @SWG\Property(property="name", type="string"),
     *                      ),
     *                      @SWG\Property(property="flightNumber", type="string"),
     *                      @SWG\Property(property="departureAirport", type="string"),
     *                      @SWG\Property(property="arrivalAirport", type="string"),
     *                      @SWG\Property(property="departureDateTime", type="datetime"),
     *                      @SWG\Property(property="arrivalDateTime", type="datetime"),
     *                      @SWG\Property(property="duration", type="integer"),
     *                  ),
     *              ),
     *         ),
     *         examples={
     *              "application/json":{
     *                  {
     *                      "searchQuery": {
     *                          "departureAirport": "IEV",
     *                          "arrivalAirport": "BUD",
     *                          "departureDate": "2018-07-01"
     *                      },
     *                      "searchResults": {
     *                          {
     *                              "transporter": {
     *                                  "code": "W6",
     *                                  "name": "WizzAir"
     *                              },
     *                              "flightNumber": "W64556",
     *                              "departureAirport": "IEV",
     *                              "arrivalAirport": "BUD",
     *                              "departureDateTime": "2018-07-01 09:30",
     *                              "arrivalDateTime": "2018-07-01 12:10",
     *                              "duration": 100
     *                          },
     *                          {
     *                              "transporter": {
     *                                  "code": "PS",
     *                                  "name": "UkraineInternational"
     *                              },
     *                              "flightNumber": "PS1234",
     *                              "departureAirport": "IEV",
     *                              "arrivalAirport": "BUD",
     *                              "departureDateTime": "2018-07-01 10:00",
     *                              "arrivalDateTime": "2018-07-01 12:35",
     *                              "duration": 95
     *                          },
     *                          {
     *                              "transporter": {
     *                                  "code": "W6",
     *                                  "name": "WizzAir"
     *                              },
     *                              "flightNumber": "W64558",
     *                              "departureAirport": "IEV",
     *                              "arrivalAirport": "BUD",
     *                              "departureDateTime": "2018-07-01 18:00",
     *                              "arrivalDateTime": "2018-07-01 20:30",
     *                              "duration": 90
     *                          }
     *                      }
     *                  }
     *              }
     *          }
     *     ),
     * )
     * @Rest\Post("/search")
     *
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return Response
     */
    public function searchAction(Request $request, ValidatorInterface $validator):Response
    {
        $rawRequest = json_decode($request->getContent(), true);
        if (!isset($rawRequest['searchQuery'])) {
            return $this->handleView($this->view([
                'success' => false,
                'message' => 'Can\'t find \'searchQuery\' element'
            ], Response::HTTP_BAD_REQUEST));
        }

        $searchRequest = new SearchRequest();
        $searchRequest->setSearchQuery($rawRequest['searchQuery']);

        $errors = $validator->validate($searchRequest);

        if (count($errors) > 0) {
            return $this->handleView($this->view([
                'success' => false,
                'message' => (string)$errors
            ], Response::HTTP_BAD_REQUEST));
        }

        $processor = new SearchProcessor($this->getDoctrine());
        $searchResponse = $processor->searchBy2AirportsAndDate($searchRequest);

        return $this->handleView($this->view($searchResponse->getResult()));
    }

    /**
     * @param string $class
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    public function getRepository(string $class):ObjectRepository
    {
        if ($this->doctrine) {
            return $this->doctrine->getRepository($class);
        } else {
            return $this->getDoctrine()->getRepository($class);
        }
    }
}