<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FrontendController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function rootAction()
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'html');

        return $this->render('base.html.twig', [], $response);
    }
}