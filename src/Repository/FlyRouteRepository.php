<?php
namespace App\Repository;

use App\Entity\FlyRouteEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method FlyRouteEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method FlyRouteEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method FlyRouteEntity[]    findAll()
 * @method FlyRouteEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FlyRouteRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, FlyRouteEntity::class);
    }

    /**
     * Search routes with specific two airports and specific day when we must depart and arrive
     *
     * @param string $departure
     * @param string $arrive
     * @param \DateTime $date
     * @param int $limit
     * @param int $offset
     * @return mixed
     */
    public function getRoutesBy2AirportsAndDate(
        string $departure,
        string $arrive,
        \DateTime $date,
        int $limit = 50,
        int $offset = 0
    ) {
        $cleanDay = $date->format('Y-m-d');

        if ($limit > 50) {
            $limit = 50;
        }

        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select('fr')
            ->from(FlyRouteEntity::class, 'fr')
            ->where($qb->expr()->eq('fr.departAirport', ':departAirport'))
            ->andWhere($qb->expr()->eq('fr.arrAirport', ':arrAirport'))
            ->andWhere($qb->expr()->gt('fr.departTime', ':departTime'))
            ->andWhere($qb->expr()->lt('fr.arrTime', ':arrTime'))
            ->setParameter('departAirport', $departure)
            ->setParameter('arrAirport', $arrive)
            ->setParameter('departTime', $cleanDay . ' 00:00:00')
            ->setParameter('arrTime', $cleanDay . ' 23:59:59')
            ->orderBy('fr.departTime', 'ASC')
            ->setMaxResults($limit)
            ->setFirstResult($offset);

        $query = $qb->getQuery();

        return $query->getResult();
    }
}