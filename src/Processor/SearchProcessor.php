<?php
namespace App\Processor;

use App\Entity\FlyRouteEntity;
use App\Repository\FlyRouteRepository;
use App\Entity\Request\Search\SearchPostEntity as SearchRequest;
use App\Entity\Response\Search\SearchPostEntity as SearchResponse;
use Doctrine\Common\Persistence\ManagerRegistry;

class SearchProcessor
{
    /**
     * @var ManagerRegistry $doctrine
     */
    public $doctrine;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * Make search by 2 airports and date
     *
     * @param SearchRequest $searchRequest
     * @return SearchResponse
     */
    public function searchBy2AirportsAndDate(SearchRequest $searchRequest)
    {
        /**
         * @var FlyRouteRepository $flyRouteRepository
         */
        $flyRouteRepository = $this->doctrine->getRepository(FlyRouteEntity::class);

        $results = $flyRouteRepository->getRoutesBy2AirportsAndDate(
            $searchRequest->getSearchQuery()['departureAirport'],
            $searchRequest->getSearchQuery()['arrivalAirport'],
            new \DateTime($searchRequest->getSearchQuery()['departureDate'])
        );

        $searchResponse = new SearchResponse();
        $searchResponse->setSearchQuery($searchRequest->getSearchQuery());
        $results ? $searchResponse->setSearchResultsFromFlyRoute($results) : $searchResponse->setSearchResults([]);

        return $searchResponse;
    }
}