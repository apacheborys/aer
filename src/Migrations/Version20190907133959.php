<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190907133959 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE fly_carrier (code VARCHAR(8) NOT NULL, name VARCHAR(126) NOT NULL, PRIMARY KEY(code))');
        $this->addSql('CREATE TABLE fly_routes (flight_number VARCHAR(8) NOT NULL, carrier_id VARCHAR(8) DEFAULT NULL, depart_airport VARCHAR(4) NOT NULL, depart_time TIMESTAMP(0) WITH TIME ZONE NOT NULL, arr_airport VARCHAR(4) NOT NULL, arr_time TIMESTAMP(0) WITH TIME ZONE NOT NULL, PRIMARY KEY(flight_number))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_60B11E0421DFC797 ON fly_routes (carrier_id)');
        $this->addSql('CREATE INDEX depart_airport_idx ON fly_routes (depart_airport)');
        $this->addSql('CREATE INDEX depart_time_idx ON fly_routes (depart_time)');
        $this->addSql('CREATE INDEX arr_airport_idx ON fly_routes (arr_airport)');
        $this->addSql('CREATE INDEX arr_time_idx ON fly_routes (arr_time)');
        $this->addSql('ALTER TABLE fly_routes ADD CONSTRAINT FK_60B11E0421DFC797 FOREIGN KEY (carrier_id) REFERENCES fly_carrier (code) NOT DEFERRABLE INITIALLY IMMEDIATE');

        if (is_file(__DIR__ . DIRECTORY_SEPARATOR . 'fly_carrier.dump')) {
            $this->addSql(file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'fly_carrier.dump'));
        }
        if (is_file(__DIR__ . DIRECTORY_SEPARATOR . 'fly_route.dump')) {
            $this->addSql(file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'fly_route.dump'));
        }
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE fly_routes DROP CONSTRAINT FK_60B11E0421DFC797');
        $this->addSql('DROP TABLE fly_carrier');
        $this->addSql('DROP TABLE fly_routes');
    }
}
